#include "Log.h"
#include "stdafx.h"

#include <stdarg.h>
#include <stdio.h>
#include <Windows.h>

#define __LOGLEVEL 3
/*
Log levels :
negative = don't long anything
0 = fatal error
1 = error that don't crash the program
2 = warning
3 = everything
*/

void _Log(unsigned int level,const char * file, const char * line,const char * funcsig, const char * message)
{
	if ( (int)(level) > __LOGLEVEL && __LOGLEVEL < 3)
		return;

	char clevel[2] = { '0' + level,0 };
	OutputDebugStringA(clevel);
	OutputDebugStringA(" ");
	OutputDebugStringA(file);
	OutputDebugStringA("\t");
	OutputDebugStringA(line);
	OutputDebugStringA("\t");
	OutputDebugStringA(funcsig);
	OutputDebugStringA(" :\t");
	OutputDebugStringA(message);
	OutputDebugStringA("\n");
	
	
}

#undef __LOGLEVEL