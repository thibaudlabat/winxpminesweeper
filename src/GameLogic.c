#include "stdafx.h"
#include "GameLogic.h"
#include <time.h>

#include "Log.h"
#include <stdio.h>

int GameLogic_resetGrid(GameLogic *game)
{
	srand(time(NULL));
	game->state = 0; // 0 : jeu en cours, 1 = gagn�, 2 = perdu

	if (game->sizeX > MINESWEEPER_MAXWIDTH)
		game->sizeX = MINESWEEPER_MAXWIDTH;

	if (game->sizeY > MINESWEEPER_MAXHEIGHT)
		game->sizeY = MINESWEEPER_MAXHEIGHT;

	if (game->nMines > (game->sizeX*game->sizeY) / 2 - 1)
		game->nMines = (game->sizeX*game->sizeY) / 2 - 1;

	// vide la grille
	for (unsigned int x = 0; x<game->sizeX; ++x)
		for (unsigned int y = 0; y < game->sizeY; ++y)
		{
			game->grid[x][y] = 0b00000000;
		}

	for (unsigned int i = 0; i < game->nMines; ++i)
	{
		// coordon�es al�atoires
		unsigned int x = rand() % game->sizeX;
		unsigned int y = rand() % game->sizeY;

		if (game->grid[x][y] & 0b10000000)
			i--; // s'il y avait d�j� une bombe, on retire 1 au compteur (annule ce passage de la boucle)
		else
			game->grid[x][y] |= 0b10000000; // ajoute une bombe
	}

	// ajout dans le tableau du nombre de bombes adjacentes � chaque case
	for (int x = 0; x<game->sizeX; ++x)
		for (int y = 0; y < game->sizeY; ++y)
		{
			int counter = 0;

			if (x - 1 >= 0 && y - 1 >= 0 && game->grid[x - 1][y - 1] & 0b10000000) counter++; // haut gauche
			if (x + 1 < game->sizeX && y - 1 >= 0 && game->grid[x + 1][y - 1] & 0b10000000) counter++; // haut droite

			if (x - 1 >= 0 && y + 1 < game->sizeY  && game->grid[x - 1][y + 1] & 0b10000000) counter++; // bas gauche
			if (x + 1 < game->sizeX && y + 1 < game->sizeY && game->grid[x + 1][y + 1] & 0b10000000) counter++; // bas droite

			if (x - 1 >= 0 && game->grid[x - 1][y] & 0b10000000) counter++; // gauche
			if (x + 1 < game->sizeX && game->grid[x + 1][y] & 0b10000000) counter++; // droite

			if ( y + 1 < game->sizeY  && game->grid[x][y + 1] & 0b10000000) counter++; // bas
			if (y-1 >=0 && game->grid[x][y - 1] & 0b10000000) counter++; // haut

			game->grid[x][y] |= counter;
		}


	return 0;
}


int GameLogic_click(GameLogic *game, unsigned int x, unsigned int y)
{

	// Log le nombre de bombes autour de la case cliqu�e
	LogFormat(3, "Case : (%d,%d) ; Bombes autour : %d", x, y, game->grid[x][y] & 0b0001111);
	
	if (x > game->sizeX || y > game->sizeY)
	{
		Log(1, "x > game->sizeX || y > game->sizeY");
		return -1;
	}
	if (game->state != 0)
	{
		Log(1, "Le jeu n'est pas en cours");
		return 1;
	}

	

	

	if ( (game->grid[x][y] & 0b01000000) || (game->grid[x][y] & 0b0001000) ) // si drapeau ou d�j� d�couvert : rien faire
	{

	}

	else if (game->grid[x][y] & 0b10000000) // si bombe
	{
		game->state = 2; // perdu
		game->lastMineX = x;
		game->lastMineY = y;
		Log(3, "Boom.");
	}
			
	else // pas de bombe
	{
		game->grid[x][y] |= 0b00010000; // bit OR : ajout de la propri�t� "case d�couverte"

		// si il n'y a pas de bombes autour, activer l'effet de d�couverte en chaine
		int done = 0;
		while (!done)
		{
			done = 1;
			for (int x = 0; x < game->sizeX; ++x)
				for (int y = 0; y < game->sizeY; ++y)
				{
					if ((game->grid[x][y] & 0b00001111) == 0 && (game->grid[x][y] & 0b00010000) != 0) // d�couverte et pas de bombes autour
					{
						 
#if 0
#define DO_THE_REPETITIVE_JOB(relX,relY) if (x + relX < game->sizeX && y + relY < game->sizeY && x + relX >= 0 && y + relY >= 0 && !(game->grid[x + relX][y + relY] & 0b00010000)){\
game->grid[x + relX][y + relY] |= 0b00010000;\
done = 0;}
#endif

#define DO_THE_REPETITIVE_JOB(relX,relY) if (x + relX < game->sizeX && y + relY < game->sizeY && x + relX >= 0 && y + relY >= 0 && !(game->grid[x + relX][y + relY] & 0b00010000)){\
game->grid[x + relX][y + relY] =(game->grid[x + relX][y + relY]|0b00010000) & (~0b01100000);\
done = 0;}

// le " & (~0b01100000) " ajout� c'est pour enlever le drapeau/[?] sur la case d�couverte avec l'effet de chaine

						DO_THE_REPETITIVE_JOB(-1, -1)
							DO_THE_REPETITIVE_JOB(-1, 0)
							DO_THE_REPETITIVE_JOB(-1, 1)

							DO_THE_REPETITIVE_JOB(0, -1)
							DO_THE_REPETITIVE_JOB(0, 1)

							DO_THE_REPETITIVE_JOB(1, -1)
							DO_THE_REPETITIVE_JOB(1, 0)
							DO_THE_REPETITIVE_JOB(1, 1)
#undef DO_THE_REPETITIVE_JOB
					}
				}
		}



		}

	if (game->state == 0) // a-t-on gagn� ?
	{
		int ok = 1;
		for (int x = 0; x < game->sizeX; ++x)
			if (ok)
				for (int y = 0; y < game->sizeY; ++y)
				{
					if (!(game->grid[x][y] & 0b10000000 || game->grid[x][y] & 0b00010000)) // si pas (bomb ou discovered)
					{
						ok = 0;
						break;
					}
				}


		if (ok)
		{
			Log(3, "Won.");

			// on met un drapeau sur chaque bombe pour l'affichage final
			for (int x = 0; x < game->sizeX; ++x)
					for (int y = 0; y < game->sizeY; ++y)
						if (game->grid[x][y] & 0b10000000) // if bomb
							game->grid[x][y] |= 0b01000000; // set flag
						
			game->state = 1;
		}
	}


	return 0;
}

int GameLogic_flagClick(GameLogic *game, unsigned int x, unsigned int y)
{
	if (game->state != 0)
		return -1;

	if (x >= game->sizeX || y >= game->sizeY)
		return -2;

	if (game->grid[x][y] & 0b00010000) // case d�couverte
	{
		game->grid[x][y] & (~0b01100000); // plus de drapeau ni de ? (s�curit�)
		return -3;
	}

	if (game->grid[x][y] & 0b01000000) // d�j� un drapeau
	{
		game->grid[x][y] ^= 0b01000000; // plus de drapeau
		game->grid[x][y] |= 0b00100000; // [?]
	}
	else if (game->grid[x][y] & 0b00100000) // pas de drapeau, [?]
	{
		game->grid[x][y] ^= 0b00100000; // plus de [?]
	}
	else
	{
		game->grid[x][y] |= 0b01000000; // ajoute un drapeau
	}

	return 0;
}


void GameLogic_reset(GameLogic * game, int newX, int newY, int newMines)
{
	game->sizeX = newX;
	game->sizeY = newY;
	game->nMines = newMines;
	GameLogic_resetGrid(game);
}

unsigned int GameLogic_flagCount(GameLogic * game)
{
	unsigned int n = 0;
	for (int x = 0; x < game->sizeX; ++x)
		for (int y = 0; y < game->sizeY; ++y)
			if (game->grid[x][y] & 0b01000000) // if flag is set
				n++;

	return n;
}