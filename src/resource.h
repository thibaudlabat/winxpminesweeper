// ?

#define IDR_MAINFRAME 401
#define IDC_STATIC -1

// icons
#define IDI_MINESWEEPER 2 
#define IDI_SMALL       3

// menus
#define IDM_NEW 101
#define  IDM_BEGINNER 102
#define  IDM_INTERMEDIATE 103
#define  IDM_EXPERT 104
#define  IDM_CUSTOM 105
#define  IDM_MARKS 106
#define  IDM_COLOR 107
#define  IDM_SOUND  108
#define  IDM_BESTTIMES 109 
#define IDM_EXIT 110
#define  IDM_CONTENTS 111 // unused
#define  IDM_SEARCHFORHELPON 112 // unused
#define  IDM_USINGHELP 113 // unused
#define  IDM_ABOUT 114

// dialogs
#define IDD_ABOUTBOX 201

// strings /class names
#define IDC_MINESWEEPER 301
#define IDS_APP_TITLE 302

