#ifndef GAME_H
#define GAME_H

#define MINESWEEPER_MAXWIDTH 300
#define MINESWEEPER_MAXHEIGHT 240


typedef unsigned char Case;
// 8 bit : 
// d�compte en partant du bit de poids fort (tout � gauche en g�n�ral)
// 1er bit = vide(0), bombe(1)
// 2eme bit = rien(0), drapeau(1)
// 3eme bit = rien(0), ?(1)
// 4eme bit = non d�couvert(0), d�couvert(1) (clic gauche!)
// [5,6,7,8]�mes bits : nombre de bombes autour (2^4 = 16 possibilit�s, en pratique �a sera de 0 � 8 inclus. (9 valeurs)

struct GameLogic
{
	Case grid[MINESWEEPER_MAXWIDTH][MINESWEEPER_MAXHEIGHT];
	unsigned int sizeX;
	unsigned int sizeY;
	unsigned int nMines;

	unsigned int lastMineX;
	unsigned int lastMineY;

	unsigned int state;
	// 0 = jeu en cours
	// 1 = jeu gagn�
	// 2 = jeu perdu


} ;
typedef struct GameLogic GameLogic;

int GameLogic_resetGrid(GameLogic *game);
int GameLogic_click(GameLogic *game, unsigned int x, unsigned int y);
void GameLogic_reset(GameLogic * game, int newX, int newY, int newMines);

#endif
