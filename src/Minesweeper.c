/*
TODO :
- Code clean et commenté, divisé de façon logique dans des fichiers
- Son
- Mode noir et blanc
- Pack toutes les images en ressources
- Pixel perfect : y'a encore des différences avec l'original sur les marges etc
- Easter Egg : mode Nightmare : prend automatiquement la taille maximale de l'écran (avec un peu de marge !) ; au clic sur une bombe, GROS bruit d'explosion FORT, avec un dessin sur l'écran entier à coup de GetDC(NULL) et dessin hors de la fenêtre

*/
#include "stdafx.h"
#include <windowsx.h>
#include "Minesweeper.h"
#include "GameLogic.h"
#define MAX_LOADSTRING 100
#include "Log.h"
#include "../res/resource.h"
#include <time.h>

// Variables globales
HINSTANCE hInst;                                // instance actuelle
WCHAR szWindowClass[MAX_LOADSTRING];            // le nom de la classe de fenêtre principale
WCHAR szTitle[MAX_LOADSTRING];					// window title

// prédéclarations
void Draw(HWND hwnd, struct Game * game);

ATOM                MyRegisterClass(HINSTANCE hInstance);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

struct Game
{
	GameLogic logic;

	HBITMAP hBitmapDigits[2];
	HBITMAP hBitmapGame[2];
	HBITMAP hBitmapEmojis[2];
	int blackAndWhite; // 0 = colored, 1 = black and white

	unsigned int leftButtonDownOnMines;
	unsigned int leftButtonDownOnReset;
	unsigned int leftButtonDown;

	unsigned char leftCounter[3];

	unsigned char rightCounter[3];
	clock_t timer;
	int timerStarted;
	int timerStopped;

	int mouseX;
	int mouseY;

	unsigned int emoji; // 0 = cliqué, 1 = gagné, 2 = perdu, 3 = grille cliquée, 4 = rien  
};
typedef struct Game Game;
Game game;

void Game_refreshEmoji(HWND * hWnd)
{
	POINT p;
	if (GetCursorPos(&p) && ScreenToClient(hWnd, &p))
	{
		// si sur le bouton restart
		if (game.leftButtonDownOnReset && p.x >= (16 + 16 * game.logic.sizeX - 24) / 2 && p.x <= (16 + 16 * game.logic.sizeX - 24) / 2 + 24 &&
			p.y >= 13 && p.y <= 13 + 24)
		{
			game.emoji = 0;
		}
		else if (game.leftButtonDown)
		{
			game.emoji = 3;
		}
		else
		{
			game.emoji = 4;
		}
	}

	if (game.logic.state == 1)
		game.emoji = 1;
	else if (game.logic.state == 2)
		game.emoji = 2;
}

void Game_refreshCounters()
{
	// compteur de drapeaux
	int flagsRemaining = game.logic.nMines - GameLogic_flagCount(&game.logic);
	char negative = (flagsRemaining < 0); // signe du nombre de "drapeaux restants"
	flagsRemaining *= (flagsRemaining < 0) ? -1 : 1; // valeur absolue

	if(!negative)
	    game.leftCounter[0] = (flagsRemaining % 1000 - flagsRemaining % 100) / 100;
	else
		game.leftCounter[0] = 11; // "-"

	game.leftCounter[1] = (flagsRemaining % 100 - flagsRemaining % 10) / 10;
	game.leftCounter[2] = (flagsRemaining % 10);

	// compteur de temps
	if (!game.timerStopped) {
		if (game.timerStarted)
		{
			clock_t end = clock();
			int seconds = (int)((float)(end - game.timer) / CLOCKS_PER_SEC);
			game.rightCounter[0] = (seconds % 1000 - seconds % 100) / 100;
			game.rightCounter[1] = (seconds % 100 - seconds % 10) / 10;
			game.rightCounter[2] = (seconds % 10);
		}
		else
		{
			game.rightCounter[0] = 0;
			game.rightCounter[1] = 0;
			game.rightCounter[2] = 0;
		}
	}

	//LogFormat(3, "Timer %d %d %d", game.rightCounter[0], game.rightCounter[1], game.rightCounter[2]);
	//LogFormat(3, "Flags %d %d %d", game.leftCounter[0], game.leftCounter[1], game.leftCounter[2]);
}

void Game_loadResources()
{
	game.hBitmapDigits[0] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_digits), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	game.hBitmapDigits[1] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_digitsbw), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	game.hBitmapGame[0] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_game), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	game.hBitmapGame[1] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_gamebw), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);

	game.hBitmapEmojis[0] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_emojis), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	game.hBitmapEmojis[1] = (HBITMAP)LoadBitmapA(hInst, MAKEINTRESOURCEA(IDB_BITMAP_emojisbw), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
}

void Game_unloadResources()
{
	DeleteObject(game.hBitmapDigits[0]);
	DeleteObject(game.hBitmapDigits[1]);
	DeleteObject(game.hBitmapGame[0]);
	DeleteObject(game.hBitmapGame[1]);
	DeleteObject(game.hBitmapEmojis[0]);
	DeleteObject(game.hBitmapEmojis[1]);
}



void Game_reset(int x, int y , int mines)
{
	// prépare le jeu
	game.leftButtonDownOnMines = 0;
	game.leftButtonDownOnReset = 0;
	game.leftButtonDown = 0;
	game.blackAndWhite = 0;
	game.emoji = 4;
	game.timerStarted = 0;
	game.timerStopped = 0;
;	GameLogic_reset(&game.logic, x, y, mines);
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR    lpCmdLine, _In_ int       nCmdShow) 
{
	Game_reset(9, 9, 10);

    // Initialise les chaînes globales
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MINESWEEPER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

	hInst = hInstance; // Stocke le handle d'instance dans la variable globale

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT, 800, 600, NULL, NULL, hInstance, NULL);

	if (!hWnd) {

		return FALSE;
	}
		

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);


	// initialise le Timer
	SetTimer(
		hWnd,
		1,
		1001,
		NULL
	);

	// beginner
	CheckMenuItem(GetMenu(hWnd), IDM_BEGINNER, MF_CHECKED);
	CheckMenuItem(GetMenu(hWnd), IDM_INTERMEDIATE, 0);
	CheckMenuItem(GetMenu(hWnd), IDM_EXPERT, 0);
	CheckMenuItem(GetMenu(hWnd), IDM_CUSTOM, 0);

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MINESWEEPER));
    MSG msg;

    while (GetMessage(&msg, NULL, 0, 0)) // mainloop
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

	
    return (int) msg.wParam;
}


ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MINESWEEPER));
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MINESWEEPER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	
    switch (message)
    {


	case WM_TIMER:
	{
		// redraw
		InvalidateRect(hWnd, NULL, TRUE);
		UpdateWindow(hWnd);
	}
	break;

	case WM_CREATE:
	{
		Game_loadResources();
		resizeWindow_auto(hWnd, &game);
	}
		break;

	case WM_LBUTTONUP:
	{
		

		POINT p;
		if (GetCursorPos(&p) && ScreenToClient(hWnd, &p))
		{
			// si dans la zone des mines
			if (!game.leftButtonDownOnReset && game.leftButtonDownOnMines && p.x > 9 && p.x < game.logic.sizeX * 16 + 9 &&
				p.y>52 && p.y < game.logic.sizeY * 16 + 52)
			{
				// gestion du timer (compteur en haut à droite)
				if (!game.timerStarted)
				{
					game.timer = clock();
					game.timerStarted = 1;
				}
				

				unsigned x = (p.x - 9) / 16;
				unsigned y = (p.y - 52) / 16;

				if (x < game.logic.sizeX && y < game.logic.sizeY)
					GameLogic_click(&game, x, y);


				if (game.logic.state!=0)
				{
					game.timerStopped = 1;
				}
			}

			// si sur le bouton restart
			if (!game.leftButtonDownOnMines && p.x >= (16 + 16 * game.logic.sizeX - 24) / 2 && p.x <= (16 + 16 * game.logic.sizeX - 24) / 2 + 24 &&
				p.y >= 13 && p.y <= 13 + 24)
			{
				// Nouvelle partie
				Log(3, "Nouvelle partie (CLICK)");
				Game_reset(game.logic.sizeX, game.logic.sizeY, game.logic.nMines);
			}
		}
		
		game.leftButtonDownOnMines = 0;
		game.leftButtonDownOnReset = 0;
		game.leftButtonDown = 0;


		// redraw
		InvalidateRect(hWnd, NULL, TRUE);
		UpdateWindow(hWnd);

		
	}
	break;

	case WM_LBUTTONDOWN:
	{		
		POINT p;
		if (GetCursorPos(&p) && ScreenToClient(hWnd, &p))
		{
			game.leftButtonDown = 1;
			// si dans la zone des mines
			if (p.x > 9 && p.x < game.logic.sizeX * 16 + 9 &&
				p.y>52 && p.y < game.logic.sizeY * 16 + 52)
			{
				game.leftButtonDownOnMines = 1;
			}

			// si sur le bouton restart
			if (p.x >= (16 + 16 * game.logic.sizeX - 24) / 2 && p.x <= (16 + 16 * game.logic.sizeX - 24) / 2 + 24 &&
				p.y>=13 && p.y <= 13+24)
			{
				game.leftButtonDownOnReset = 1;
			}
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);

		}
	}
	break;

	case WM_RBUTTONDOWN:
	{
		POINT p;
		if (GetCursorPos(&p) && ScreenToClient(hWnd, &p))
		{
			// si dans la zone des mines
			if (p.x > 9 && p.x < game.logic.sizeX * 16 + 9 &&
				p.y>52 && p.y < game.logic.sizeY * 16 + 52)
			{
				unsigned x = (p.x - 9) / 16;
				unsigned y = (p.y - 52) / 16;

				if (x < game.logic.sizeX && y < game.logic.sizeY)
					GameLogic_flagClick(&game, x, y);
			}
		}

		InvalidateRect(hWnd, NULL, TRUE);
		UpdateWindow(hWnd);
	}


	break;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Analyse les sélections de menu :
            switch (wmId)
            {

			case IDM_NEW:
				// Nouvelle partie
				Log(3, "Nouvelle partie (IDM_NEW)");
				Game_reset(game.logic.sizeX, game.logic.sizeY, game.logic.nMines);
				break;

			case IDM_BEGINNER:
				CheckMenuItem(GetMenu(hWnd), IDM_BEGINNER, MF_CHECKED);
				CheckMenuItem(GetMenu(hWnd), IDM_INTERMEDIATE, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_EXPERT, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_CUSTOM, 0);
				Log(3, "Beginner");
				Game_reset(9, 9, 10);
				resizeWindow_auto(hWnd, &game);
				break;

			case IDM_INTERMEDIATE:
				CheckMenuItem(GetMenu(hWnd), IDM_BEGINNER, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_INTERMEDIATE, MF_CHECKED);
				CheckMenuItem(GetMenu(hWnd), IDM_EXPERT, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_CUSTOM, 0);
				Log(3, "Intermediate");
				Game_reset(16, 16, 40);
				resizeWindow_auto(hWnd, &game);
				break;

			case IDM_EXPERT:
				CheckMenuItem(GetMenu(hWnd), IDM_BEGINNER, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_INTERMEDIATE, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_EXPERT, MF_CHECKED);
				CheckMenuItem(GetMenu(hWnd), IDM_CUSTOM, 0);
				Log(3, "Expert");
				Game_reset(30, 16, 99);
				resizeWindow_auto(hWnd, &game);
				break;

				/*
			case IDM_CUSTOM:
				MessageBox(hWnd, L"not implemented", L".",MB_ICONINFORMATION);
				CheckMenuItem(GetMenu(hWnd), IDM_BEGINNER, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_INTERMEDIATE, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_EXPERT, 0);
				CheckMenuItem(GetMenu(hWnd), IDM_CUSTOM, MF_CHECKED);
				Log(3, "Custom");
				Game_reset(110, 50, 500);
				resizeWindow_auto(hWnd, &game);
				break;

			case IDM_MARKS:
				MessageBox(hWnd, L"not implemented", L".", MB_ICONINFORMATION);
				break;
				*/
			case IDM_COLOR:
				game.blackAndWhite = 1 - game.blackAndWhite;
				break;
				/*
			case IDM_SOUND:
				MessageBox(hWnd, L"not implemented", L".", MB_ICONINFORMATION);
				break;

			case IDM_BESTTIMES:
				MessageBox(hWnd, L"not implemented", L".", MB_ICONINFORMATION);
				break;
				*/

			// ------------

            case IDM_ABOUT:
				MessageBoxW(hWnd, L"See https://gitlab.com/thibaudlabat/winxpminesweeper", L"About", MB_ICONINFORMATION);
				Log(3, "IDM_ABOUT");
                break;

            case IDM_EXIT:
                DestroyWindow(hWnd);
				return 0;

            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
			
			// redraw
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);
        }
        break;

	case WM_MOUSEMOVE:
	{
		game.mouseX = GET_X_LPARAM(lParam);
		game.mouseY = GET_Y_LPARAM(lParam);

		if (game.leftButtonDownOnMines || game.leftButtonDownOnReset)
		{
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);
		}
	}
	break;

    case WM_PAINT:
		Draw(hWnd,&game);
        break;

    case WM_DESTROY:
		Game_unloadResources();
        PostQuitMessage(0);
        break;

	case WM_ERASEBKGND:// prevent flickering
		return TRUE; // tell Windows that we handled it. (but don't actually draw anything)

    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }


    return 0;
}

void Draw_1(Game * game, HDC hdc)
{
	// fond

		HPEN hPen1 = CreatePen(PS_SOLID, 1, RGB(192, 192, 192));
		HPEN holdPen = SelectObject(hdc, hPen1);
		HBRUSH hBrush1 = CreateSolidBrush(RGB(192, 192, 192));
		HBRUSH holdBrush = SelectObject(hdc, hBrush1);

		Rectangle(hdc, 0, 0, 16 + 16 * game->logic.sizeX, 60 + 16 * game->logic.sizeY); // fond

		HPEN hPen2 = CreatePen(PS_SOLID, 2, RGB(128, 128, 128)); // contours de la bande haute
		SelectObject(hdc, hPen2);
		MoveToEx(hdc, 7, 43, NULL);
		LineTo(hdc, 7, 7);
		LineTo(hdc, 10 + game->logic.sizeX * 16, 7);

		HPEN hPen3 = CreatePen(PS_SOLID, 2, RGB(255, 255, 255));
		SelectObject(hdc, hPen3);
		LineTo(hdc, 10 + game->logic.sizeX * 16, 42);
		LineTo(hdc, 10, 42);

		// contours zone des mines
		HPEN hPen4 = CreatePen(PS_SOLID, 3, RGB(128, 128, 128));
		SelectObject(hdc, hPen4);
		MoveToEx(hdc, 7, 53 + 16 * game->logic.sizeY, NULL);
		LineTo(hdc, 7, 50);
		LineTo(hdc, 10 + game->logic.sizeX * 16, 50);

		HPEN hPen5 = CreatePen(PS_SOLID, 3, RGB(255, 255, 255));
		SelectObject(hdc, hPen5);
		LineTo(hdc, 10 + game->logic.sizeX * 16, 53 + 16 * game->logic.sizeY);
		LineTo(hdc, 7, 53 + 16 * game->logic.sizeY);

		//HPEN hPen6 = CreatePen(PS_INSIDEFRAME, 0, RGB(0, 0, 0));
		//SelectObject(hdc, hPen6);
		//HBRUSH hBrush6 = CreateSolidBrush(RGB(255, 0, 0));

		//SelectObject(hdc, hBrush6);


		SelectObject(hdc, holdPen);
		SelectObject(hdc, holdBrush);
		DeleteObject(hBrush1);
		//DeleteObject(hBrush6);
		DeleteObject(hPen1);
		DeleteObject(hPen2);
		DeleteObject(hPen3);
		DeleteObject(hPen4);
		DeleteObject(hPen5);
		//DeleteObject(hPen6);
}

void Draw_2(Game * game, HDC hdc, HWND hwnd)
{
	// grille et mines

    HBITMAP hBitmap = game->hBitmapGame[game->blackAndWhite]; // alias
	BITMAP bitmap;
	HDC hdcMem;
	HGDIOBJ oldBitmap;


	hdcMem = CreateCompatibleDC(hdc);
	oldBitmap = SelectObject(hdcMem, hBitmap);

	GetObject(hBitmap, sizeof(bitmap), &bitmap);
	for (unsigned int x = 0; x < game->logic.sizeX; ++x)
		for (unsigned int y = 0; y < game->logic.sizeY; ++y)
		{
			if (game->logic.grid[x][y] & 0b00010000) // découvert
			{
				// calcul de l'image à dessiner en fonction du "numéro" de la case (bombes adjacentes)
				int k = game->logic.grid[x][y] & 0b00001111;
				BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, (15 - k) * 16, SRCCOPY);
			}
			else if (game->logic.grid[x][y] & 0b01000000)// pas découvert, avec drapeau
			{
				BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, 1 * 16, SRCCOPY);
			}
			else if (game->logic.grid[x][y] & 0b00100000)// pas drapeau, [?]
			{
				BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, 2 * 16, SRCCOPY);
			}
			else // case sans rien
			{
				BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, 0 * 16, SRCCOPY);
			}

			if (game->logic.state == 2 && game->logic.grid[x][y] & 0b10000000)
			{
				BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, 5 * 16, SRCCOPY);
			}
		}
	if (game->logic.state == 2) // / bombe rouge sur laquelle tu as cliqué
	{
		BitBlt(hdc, 9 + 16 * game->logic.lastMineX, 52 + 16 * game->logic.lastMineY, bitmap.bmWidth, 16, hdcMem, 0, 3 * 16, SRCCOPY);
	}

	if (game->logic.state == 0 && game->leftButtonDownOnMines) // jeu pas terminé et bouton enfoncé sur les mines
	{
		
		unsigned int x = (game->mouseX - 9) / 16,
			y = (game->mouseY - 52) / 16;

		if ((game->mouseX > 9 && game->mouseX < game->logic.sizeX * 16 + 9 &&
			game->mouseY>52 && game->mouseY < game->logic.sizeY * 16 + 52)
			&& !(game->logic.grid[x][y] & 0b01010000))
		{
			BitBlt(hdc, 9 + 16 * x, 52 + 16 * y, bitmap.bmWidth, 16, hdcMem, 0, 15 * 16, SRCCOPY);
		}

	}

		

	SelectObject(hdcMem, oldBitmap);
	DeleteDC(hdcMem);

	
}

void Draw_3(Game * game,HDC hdc)
{
		// smiley en haut

	 //contour

		HPEN hPen1 = CreatePen(PS_SOLID, 1, RGB(128, 128, 128));
		HPEN holdPen = SelectObject(hdc, hPen1);
		HBRUSH hBrush1 = CreateSolidBrush(RGB(128, 128, 128));
		HBRUSH holdBrush = SelectObject(hdc, hBrush1);

		Rectangle(hdc, (16 + 16 * game->logic.sizeX - 24) / 2 - 1,
			13 - 1,
			(16 + 16 * game->logic.sizeX - 24) / 2 + 24 + 1,
			13 + 24 + 1); // fond

		SelectObject(hdc, holdPen);
		SelectObject(hdc, holdBrush);
		DeleteObject(hPen1);
		DeleteObject(hBrush1);


		// smiley bitmap


		HBITMAP hBitmap = game->hBitmapEmojis[game->blackAndWhite]; // alias
		BITMAP bitmap;
		HDC hdcMem;
		HGDIOBJ oldBitmap;


		hdcMem = CreateCompatibleDC(hdc);
		oldBitmap = SelectObject(hdcMem, hBitmap);

		GetObject(hBitmap, sizeof(bitmap), &bitmap);
		BitBlt(hdc, (16 + 16 * game->logic.sizeX - 24) / 2, 13, bitmap.bmWidth, 24, hdcMem, 0, game->emoji * 24, SRCCOPY);
		SelectObject(hdcMem, oldBitmap);
		DeleteDC(hdcMem);
}

void Draw_4(Game * game, HDC hdc)
{
	// Compteurs

	//contour

	HPEN hPen1 = CreatePen(PS_SOLID, 1, RGB(128, 128, 128));
	HPEN holdPen = SelectObject(hdc, hPen1);
	HBRUSH hBrush1 = CreateSolidBrush(RGB(128, 128, 128));
	HBRUSH holdBrush = SelectObject(hdc, hBrush1);

	// faudra les faire !

	SelectObject(hdc, holdPen);
	SelectObject(hdc, holdBrush);
	DeleteObject(hPen1);
	DeleteObject(hBrush1);


	// bitmap


	HBITMAP hBitmap = game->hBitmapDigits[game->blackAndWhite]; // alias
	BITMAP bitmap;
	HDC hdcMem;
	HGDIOBJ oldBitmap;


	hdcMem = CreateCompatibleDC(hdc);
	oldBitmap = SelectObject(hdcMem, hBitmap);

	GetObject(hBitmap, sizeof(bitmap), &bitmap);

	// gauche
	BitBlt(hdc, 18, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->leftCounter[0]) * 23, SRCCOPY);
	BitBlt(hdc, 18+13, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->leftCounter[1]) * 23, SRCCOPY);
	BitBlt(hdc, 18+2*13, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->leftCounter[2]) * 23, SRCCOPY);

	// droit
	int x_ = 16 + 16 * game->logic.sizeX - 3*13 - 18;
	BitBlt(hdc, x_, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->rightCounter[0]) * 23, SRCCOPY);
	BitBlt(hdc, x_ + 13, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->rightCounter[1]) * 23, SRCCOPY);
	BitBlt(hdc, x_ + 2 * 13, 13, bitmap.bmWidth, 23, hdcMem, 0, (11 - game->rightCounter[2]) * 23, SRCCOPY);

	SelectObject(hdcMem, oldBitmap);
	DeleteDC(hdcMem);
}
void Draw(HWND hwnd, struct Game * game) 
{// buffered
	
	Game_refreshCounters();
	Game_refreshEmoji(hwnd);

	RECT r;
	GetClientRect(hwnd, &r);

	// Get a device context to the screen.
	HDC buffer_hdcScreen = GetDC(NULL);

	// Create a device context
	HDC buffer_hdcBmp = CreateCompatibleDC(buffer_hdcScreen);

	// Create a bitmap and attach it to the device context we created above...
	HBITMAP buffer_bmp = CreateCompatibleBitmap(buffer_hdcScreen, r.right, r.bottom);
	HBITMAP buffer_hbmOld = (HBITMAP)SelectObject(buffer_hdcBmp, buffer_bmp);

	// Now, you can draw into bmp using the device context hdcBmp...
	//////////////////////////////////
	
	Draw_1(game, buffer_hdcBmp);
	Draw_2(game, buffer_hdcBmp,hwnd);
	Draw_3(game, buffer_hdcBmp);
	Draw_4(game, buffer_hdcBmp);
	
	


	SelectObject(buffer_hdcBmp, buffer_hbmOld);
	DeleteDC(buffer_hdcBmp);
	ReleaseDC(NULL, buffer_hdcScreen);
	
	
	{ // copie du buffer à l'écran
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hwnd, &ps);

		HDC hdcScreen = GetDC(NULL);
		HDC hdcSrc = CreateCompatibleDC(hdcScreen);
		HBITMAP hbmOld = (HBITMAP)SelectObject(hdcSrc, buffer_bmp);

		BitBlt(hdc, 0, 0, r.right, r.bottom, hdcSrc, 0, 0, SRCCOPY);

		SelectObject(hdcSrc, hbmOld);
		DeleteDC(hdcSrc);
		ReleaseDC(NULL, hdcScreen);
		EndPaint(hwnd, &ps);
	}
}

BOOL resizeWindow_auto(HWND hWnd, Game * game)
{
	
#if 0
	RECT windowRect;
	GetWindowRect	(hWnd, &windowRect);

	RECT clientRect;
	GetClientRect(hWnd, &clientRect);
	// valeurs 32 et 119 de la fonction en dessous trouvées à taton avec les commandes ci-dessus, faudrait un truc dynamique !! --> TODO
#endif
	
	return SetWindowPos(hWnd, 0, 0, 0, 32 + 16 * game->logic.sizeX , 119 + 16 * game->logic.sizeY, SWP_NOMOVE | SWP_NOZORDER | SWP_NOACTIVATE);
	

}
