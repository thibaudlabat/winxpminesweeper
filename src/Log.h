#pragma once


// conversion int->char* 
#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)


// short file name
//https://stackoverflow.com/a/8488201
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

// ajout des macros : fichier, ligne , fonction
#define Log(level,message) _Log(level,__FILENAME__,STR(__LINE__),__FUNCSIG__,message)

void _Log(unsigned int level, const char * file, const char * line, const char * funcsig, const char * message);




// Pareil mais avec un formattage du message par sprintf_s
#define LogFormat_bufferSize 2048
static char LogFormat_buffer[LogFormat_bufferSize];
#define LogFormat(level,message, ...) {sprintf_s(LogFormat_buffer, LogFormat_bufferSize, message, __VA_ARGS__);Log(level,LogFormat_buffer);}

/* 
LogFormat(3, "%d %d %d", a,b,c);

est equivalent � :

static char buffer[2048];
sprintf_s(buffer, 2048, "%d %d %d", a,b,c);
Log(3,buffer);

*/



